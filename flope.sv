module flope #(parameter WIDTH = 8)
            (input logic clk, reset, enable,
              input logic [WIDTH-1:0] d,
              output logic [WIDTH-1:0] q);
	
  initial
    begin
      q <= 0;
    end

  always_ff @(posedge clk)
	begin
		if (reset) q <= 0;
		else if(enable) q <= d;
	end
endmodule