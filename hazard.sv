module hazard(input logic [31:0] instrD,instrE,
					input logic [4:0] writeRegM,writeRegW,
					input logic regWriteM, regWriteW,
					input logic memtoregE,
					output logic forwardBD, forwardAD,
					output logic [1:0] forwardBE, forwardAE,
					output logic flushE, stallD, stallF);

logic [4:0] rsD, rtD, rsE, rtE;
logic lwstall;

assign rsD = instrD[25:21];
assign rtD = instrD[20:16];
assign rsE = instrE[25:21];
assign rtE = instrE[20:16];					

assign lwstall = ((rsD == rtE) || (rtD == rtE)) && memtoregE;
assign stallF = lwstall;
assign stallD = lwstall;
assign slushE = lwstall;

always_comb
	begin
	if ((rsD !== 0) && (rsD == writeRegW) && regWriteW)
		forwardAD <= 1;
	else
		forwardAD <= 0;
	if ((rtD !== 0) && (rtD == writeRegW) && regWriteW)
		forwardBD <= 1;
	else
		forwardBD <= 0;
	if ((rsE !== 0) && (rsE == writeRegM) && regWriteM)
		forwardAE <= 10;
	else if ((rsE !== 0) && (rsE == writeRegW) && regWriteW)
		forwardAE <= 01;
	else 
		forwardAE <= 00;
	if ((rtE !== 0) && (rtE == writeRegM) && regWriteM)
		forwardBE <= 10;
	else if ((rtE !== 0) && (rtE == writeRegW) && regWriteW)
		forwardBE <= 01;
	else 
		forwardBE <= 00;
	end

endmodule
