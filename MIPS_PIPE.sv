module nullbox #(parameter WIDTH = 8)
            (input logic clk, reset,
              input logic [WIDTH-1:0] d,
              output logic [WIDTH-1:0] q);
assign q = d;

endmodule