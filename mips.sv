module mips(input logic clk, reset, output logic [31:0] pc,
            input logic [31:0] instr,
            output logic memwrite,
            output logic [31:0] aluout, writedata,
            input logic [31:0] readdata);
            
  logic memtoreg, alusrc, regdst, regwrite, branch, zero, jump;
  logic [2:0] alucontrol;
  logic [31:0] instrD;

  controller c(instrD[31:26], instrD[5:0], zero, memtoreg, memwrite,
    branch, alusrc, regdst, regwrite, alucontrol, jump);
  
  datapath dp(clk, reset, memtoreg, branch, alusrc, regdst, regwrite,
    alucontrol, zero, pc, instr, aluout, writedata, readdata, jump, instrD);

endmodule