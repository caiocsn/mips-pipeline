module top(input logic clk, reset,
            output logic [31:0] writedata, dataadr,
            output logic memwrite);

	logic [31:0] pc, instr, readdata;
	logic memwriteE, memwriteW;

// instantiate processor and memories
  mips mips(clk, reset, pc, instr, memwrite, dataadr, writedata, readdata);
  imem imem(pc[7:2], instr);
  
  // registers needed to implement the pipelining logic
  flopr #(1) memwrite_reg1(clk, reset, memwrite, memwriteE);
  flopr #(1) memwrite_reg2(clk, reset, memwriteE, memwriteW);
  dmem dmem(clk, memwriteW, dataadr, writedata, readdata);
endmodule
