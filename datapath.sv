module datapath(input logic clk, reset,
                input logic memtoreg, branch,
                input logic alusrc, regdst,
                input logic regwrite,
                input logic [2:0] alucontrol,
                output logic zeroM,
                output logic [31:0] pc,
                input logic [31:0] instr,
                output logic [31:0] aluoutM, writedataM,
                input logic [31:0] readdata,
					 input logic jump,
					 output logic [31:0] instrD);
  logic [4:0] writeregE, writeregM, writeregW;
  logic [31:0] pcnext, pcnextbr, pcbranchE, pcbranchM;
  logic [31:0] signimmD, signimmE, signimmsh;
  logic [31:0] srca, srcaE, srcaE_, srcb;
  logic [31:0] resultW;
  logic [31:0] instrE;
  logic [31:0] pcplus4, pcplus4D, pcplus4E;
  logic [31:0] writedata, writedataE, writedataE_;
  logic [31:0] aluoutE, aluoutW;
  logic [31:0] readdataW;
  logic [31:0] rd1, rd2;
  logic [2:0] alucontrolE;
  logic zeroE, regdstE, alusrcE, memtoregE, regwriteE, pcsrc;
  logic memtoregM, regwriteM;
  logic memtoregW, regwriteW;
  logic branchE;
  logic [1:0] forwardBE, forwardAE;
  logic forwardAD, forwardBD, stallF, stallD, flushE;
  
  assign pcsrc = branchE && zeroE;
  
  // next PC logic
  flope #(32) pcreg(clk, reset, ~stallF, pcnextbr, pcnext);
  adder pcadd1(pc, 32'b100, pcplus4);
  sl2 immsh(signimmE, signimmsh);
  adder pcadd2(pcplus4E, signimmsh, pcbranchE);
  mux2 #(32) pcbrmux(pcplus4, pcbranchE, pcsrc, pcnextbr);
  mux2 #(32) jumpmux(pcnext,{pcplus4D[31:28], instrD[25:0], 2'b00}, jump, pc);
  
  // register file logic
  regfile rf(clk, regwriteW, instrD[25:21], instrD[20:16], writeregW, resultW, srca, writedata);
  mux2 #(5) wrmux(instrE[20:16], instrE[15:11], regdstE, writeregE);
  mux2 #(32) resmux(aluoutW, readdataW, memtoregW, resultW);
  signext se(instrD[15:0], signimmD);

  // ALU logic
  mux2 #(32) srcbmux(writedataE_, signimmE, alusrcE, srcb);
  alu32 alu(srcaE_, srcb, alucontrolE, aluoutE, zeroE);
  
  // Hazard logic
  hazard hazard_control(instrD, instrE, writeregM, writeregW, regwriteM, regwriteW, memtoregE,
  forwardBD, forwardAD, forwardBE, forwardAE, flushE, stallD, stallF); 
  mux3 #(32) srcamux(srcaE, resultW, aluoutM, forwardAE, srcaE_);
  mux3 #(32) srcbmux_(writedataE, resultW, aluoutM, forwardBE, writedataE_);
  mux2 #(32) rd1mux(srca, resultW, forwardAD, rd1);
  mux2 #(32) rd2mux(writedata, resultW, forwardBD, rd2);
  
  // PIPELINING registers - connections,
  
  flope #(32) D_reg1(clk, reset || pcsrc, ~stallD, instr, instrD);
  flope #(32) D_reg2(clk, reset || pcsrc, ~stallD, pcplus4, pcplus4D);
  flopr #(32) E_reg1(clk, reset || flushE || pcsrc, rd1, srcaE);
  flopr #(32) E_reg2(clk, reset || flushE || pcsrc, rd2, writedataE);
  flopr #(32) E_reg3(clk, reset || flushE || pcsrc, instrD, instrE);
  flopr #(32) E_reg4(clk, reset || flushE || pcsrc, signimmD, signimmE);
  flopr #(32) E_reg5(clk, reset || flushE || pcsrc, pcplus4D, pcplus4E);
  flopr #(1) M_reg1(clk, reset, zeroE, zeroM);
  flopr #(32) M_reg2(clk, reset, aluoutE, aluoutM);
  flopr #(32) M_reg3(clk, reset, writedataE_, writedataM);
  flopr #(5) M_reg5(clk, reset, writeregE, writeregM);
  flopr #(32) W_reg1(clk, reset, readdata, readdataW);
  flopr #(32) W_reg2(clk, reset, aluoutM, aluoutW);
  flopr #(5) W_reg3(clk, reset, writeregM, writeregW);
  
  // PIPELINING registers - signals
  flopr #(1) E_singal_reg1(clk, reset || flushE || pcsrc, regdst, regdstE);
  flopr #(1) E_singal_reg2(clk, reset || flushE || pcsrc, alusrc, alusrcE);
  flopr #(3) E_singal_reg3(clk, reset || flushE || pcsrc, alucontrol, alucontrolE);
  flopr #(1) E_singal_reg4(clk, reset || flushE || pcsrc, memtoreg, memtoregE);
  flopr #(1) E_singal_reg5(clk, reset || flushE || pcsrc, regwrite, regwriteE);
  flopr #(1) E_singal_reg6(clk, reset || flushE || pcsrc, branch, branchE);
  flopr #(1) M_singal_reg1(clk, reset, memtoregE, memtoregM);
  flopr #(1) M_singal_reg2(clk, reset, regwriteE, regwriteM);
  flopr #(1) W_singal_reg1(clk, reset, memtoregM, memtoregW);
  flopr #(1) W_singal_reg2(clk, reset, regwriteM, regwriteW);
endmodule
