module flopr #(parameter WIDTH = 8)
            (input logic clk, reset,
              input logic [WIDTH-1:0] d,
              output logic [WIDTH-1:0] q);
	
  initial
    begin
      q <= 0;
    end

  always_ff @(posedge clk)
    if (reset) q <= 0;
    else q <= d;
endmodule